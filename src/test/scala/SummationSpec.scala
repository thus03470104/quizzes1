import Quizzes._
import org.scalatest.FunSuite

/**
  * Created by mark on 05/03/2017.
  */
class SummationSpec extends FunSuite{
  test("summation 1 to 10 should should be 55"){
    assert(summation(10)==55)
  }

  test("summation 1 to 1177 should be 693253"){
    assert(summation(1177)==693253)
  }

}
